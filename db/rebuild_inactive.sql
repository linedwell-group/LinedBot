-- CREATE THE TABLE
CREATE TABLE IF NOT EXISTS inactive (project TEXT, username TEXT, lastedit TEXT, status TEXT);

-- CREATE THE UNIQUE INDEX
CREATE UNIQUE INDEX IF NOT EXISTS idx_inactive ON INACTIVE (project, username);
