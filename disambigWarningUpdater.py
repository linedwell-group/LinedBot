#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Script de mise à jour semi-automatique de {{Avertissements d'homonymie restants}}
#
# (C) Linedwell, 20211-2025
#
# Distribué sous licence GNU GPLv3
# Distributed under the terms of the GNU GPLv3 license
# http://www.gnu.org/licenses/gpl.html

import sys
sys.path.insert(1, '..') #ajoute au PYTHONPATH le répertoire parent

import mylogging # pylint: disable=unused-import

import re
#import locale
#locale.setlocale(locale.LC_TIME, 'fr_fr') #remplacer 'fr_fr' par 'fra_fra' sur Windows
import datetime

import pywikibot
from typing import Iterator, Tuple, List, Dict # pylint: disable=unused-import

################
# Déclarations #
################
site = pywikibot.Site('fr', 'wikipedia')

#Compte les templates {{Avertissement Homonymie}}
def disambigWarningCount(template="Avertissement Homonymie") -> int:
    return len(list(pywikibot.Page(site, template, ns=site.namespaces.TEMPLATE).getReferences(only_template_inclusion=True)))

#Met à jour le tableau récapitulatif
def disambigWarningUpdate(pageTemp: str, newNumber: int) -> str:
    pageTemp = re.split(r'(\|-\n)', pageTemp, maxsplit=3)
    pageTempBegin = ''.join(pageTemp[:-1])
    pageTempBody = ''.join(pageTemp[-1:])
    pageTempBody = re.split(r'\|\}\n', pageTempBody)
    pageTempEnd = '|}\n' + ''.join(pageTempBody[-1:])
    pageTempBody = ''.join(pageTempBody[:-1])
    lastRaw = pageTempBody.rsplit('|-\n', 1)[1]

    rawObj = rawParser(lastRaw)
    newRaw = rawMaker(rawObj, newNumber)

    pageTemp = pageTempBegin + pageTempBody + newRaw + pageTempEnd

    return pageTemp

#############
# Functions #
#############
#Convertit une ligne en "objet" utilisable
def rawParser(raw: str) -> dict:
    rawItems = raw.split('|')

    ident = ''.join(rawItems[1].split())
    date = ''.join(rawItems[2].split())
    number = ''.join(re.findall(r'\d+', rawItems[5]))

    rawObj = {
        'id' : ident,
        'date' : date,
        'number' : number
    }
    return rawObj


#Génère la ligne supplémentaire du tableau, calculée à partir de la dernière ligne
def rawMaker(prevRaw: dict, newNumber: int) -> str:

    oldId = int(prevRaw['id'])
    oldDate = prevRaw['date']
    oldNumber = int(prevRaw['number'])

    dateObj = datetime.datetime.today()
    date = dateObj.strftime('%d-%m-%Y').lstrip('0') #suppression du 0 initial

    oldDateObj = datetime.datetime.strptime(oldDate, '%d-%m-%Y').replace(tzinfo=datetime.timezone.utc)

    durationObj = dateObj - oldDateObj
    duration = str(durationObj.days) + ' jours'

    diffSize = newNumber - oldNumber
    diffPercent = (diffSize * 100.) / oldNumber
    diffPercent = round(diffPercent, 2)

    if diffSize > 0:
        diffColor = "{{rouge|+"
    else:
        diffColor = "{{vert|"

    newRaw = "|-\n| %s\n| %s\n|| %s\n| {{formatnum:%s}} || %s%s%%}}\n|| %s%s}}\n" %((oldId+1), date, duration, newNumber, diffColor, diffPercent, diffColor, diffSize)

    return newRaw


#############
# Execution #
#############
def main():
    target = "Modèle:Avertissements d'homonymie restants"
    page = pywikibot.Page(site, target)

    try:
        pageTemp = page.get()

    except pywikibot.exceptions.NoPageError:
        pywikibot.error("Page %s does not exist; skipping."
                         % page.title(as_link=True))
    except pywikibot.exceptions.IsRedirectPageError:
        pywikibot.error("Page %s is a redirect; skipping."
                         % page.title(as_link=True))
    except pywikibot.exceptions.LockedPageError:
        pywikibot.error("Page %s is locked; skipping."
                         % page.title(as_link=True))
    else:
        newNumber = disambigWarningCount()
        pageTemp = disambigWarningUpdate(pageTemp, newNumber)

        if pageTemp != page.get():
            page.text = pageTemp
            try:
                page.save("[[WP:Bot|Robot]] : mise à jour")
            except pywikibot.exceptions.PageSaveRelatedError as myexception:
                pywikibot.error('%s %s'% (type(myexception), myexception.args))
        else:
            pywikibot.output("Aucune mise à jour n'a été faite.")

if __name__ == "__main__":
    try:
        main()
    finally:
        pywikibot.stopme()
