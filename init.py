#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Script d'initialisation des sections hebdomadaires / mensuelles
#
# (C) Linedwell, 20211-2025
#
# Distribué sous licence GNU GPLv3
# Distributed under the terms of the GNU GPLv3 license
# http://www.gnu.org/licenses/gpl.html

import sys
sys.path.insert(1, '..') #ajoute au PYTHONPATH le répertoire parent

import mylogging # pylint: disable=unused-import

from datetime import date

import pywikibot

from typing import Iterator, Tuple, List, Dict # pylint: disable=unused-import

# Variables temporelles
dt = date.today()
yr =dt.strftime('%Y')
isoyr = dt.strftime('%G')
mt = dt.strftime('%m')
isowk = dt.strftime('%V')

################
# Déclarations #
################
# Listes de pages
## Wikipedia
dicoWP = {
    'site' : pywikibot.Site('fr', 'wikipedia'),
    'pagesList' : ['Wikipédia:Bulletin des administrateurs/%s/Semaine %s' % (isoyr, str(int(isowk)))],
    'pagesHeader' : ['<noinclude>{{Wikipédia:Bulletin des administrateurs/en-tête court|année='+ isoyr + '}}</noinclude>'],
    'summary' : '[[WP:Bot|Robot]] : initialisation de sous-page périodique'
}

##Vikidia
dicoVD = {
    'site' : pywikibot.Site('fr', 'vikidia'),
    'pagesList' : ['Vikidia:Bavardages/%s/%s' % (isoyr, isowk), 'Vikidia:Bulletin des administrateurs/%s %s' % (yr, mt), 'Vikidia:Demandes aux administrateurs/%s %s' % (yr, mt), 'Vikidia:Demandes aux bureaucrates/%s %s' % (yr, mt), 'Vikidia:Le_Savant/%s_%s' % (isoyr, isowk), 'Vikidia:Demandes aux vérificateurs d\'utilisateurs/%s %s' % (yr, mt)],
    'pagesHeader' : ['{{subst:Vikidia:Bavardages/Initialisation}}', '<noinclude>{{Vikidia:Bulletin des administrateurs/Navigation|année='+ yr + '}}</noinclude>', '<noinclude>{{Vikidia:Demandes aux administrateurs/Navigation|année=' + yr + '}}</noinclude>\n__TOC__\n{{clr}}', '<noinclude>{{Vikidia:Demandes aux bureaucrates/Navigation|année=' + yr + '}}</noinclude>\n__TOC__\n{{clr}}', '<noinclude>{{Vikidia:Le Savant/En-tête}}</noinclude>','<noinclude>{{Vikidia:Demandes aux vérificateurs d\'utilisateurs/Navigation|année=' + yr + '}}</noinclude>\n__TOC__\n{{clr}}'],
    'summary' : '[[VD:Robot|Robot]] : initialisation de sous-page périodique'
}

#############
# Functions #
#############
#initialise les pages si besoin
def initPages(dico: dict):
    index = 0
    for pageName in dico['pagesList']:
        page = pywikibot.Page(dico['site'], pageName)

        if not page.exists():
            pageTemp = dico['pagesHeader'][index]
            summary = dico['summary']
            page.text = pageTemp
            try:
                page.save(summary, force=True)
            except pywikibot.exceptions.PageSaveRelatedError as myexception:
                pywikibot.error('%s %s'% (type(myexception), myexception.args))

        else:
            pywikibot.output("Page %s already exists; skipping."
                         % page.title(as_link=True))

        index += 1

#############
# Execution #
#############
def main():
    #timeStart = time.time()
    initPages(dicoWP)
    initPages(dicoVD)
    #initMonthlyPages()
    #timeEnd = time.time()

if __name__ == "__main__":
    try:
        main()
    finally:
        pywikibot.stopme()
