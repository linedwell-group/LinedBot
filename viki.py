#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Script de maintenance pour vikidia
#
# (C) Linedwell, 20211-2025
#
# Distribué sous licence GNU GPLv3
# Distributed under the terms of the GNU GPLv3 license
# http://www.gnu.org/licenses/gpl.html

import sys, getopt
sys.path.insert(1, '..') #ajoute au PYTHONPATH le répertoire parent

import mylogging # pylint: disable=unused-import

import re, time

import pywikibot
from pywikibot import pagegenerators

import callback # pylint: disable=unused-import
import logger # pylint: disable=unused-import
from typing import Iterator, Tuple, List, Dict # pylint: disable=unused-import

################
# Déclarations #
################
site = pywikibot.Site('fr', 'vikidia')
nbrModif = 0
nbrTotal = 0

#############
# Functions #
#############
# Traitement des nouvelles pages
def newPages(allPages=False) -> str:
    global nbrModif, nbrTotal # pylint: disable=global-statement

    log = ''

    homonCat =  pywikibot.Category(site, "Homonymie")

    ebaucheCat = pywikibot.Category(site, "Ébauche")
    ebaucheCat = set(ebaucheCat.subcategories(recurse=3))

    hiddenCat = pywikibot.Category(site, "Catégorie cachée")
    hiddenCat = set(hiddenCat.subcategories())

    portalCat = pywikibot.Category(site, "Liste d'articles")
    portalCat = set(portalCat.subcategories())

    ignoreCat = pywikibot.Category(site, "Page ignorée par les robots")
    ignorePage = pywikibot.Page(site, "Utilisateur:LinedBot/Ignore")
    ignoreList = list(ignorePage.linkedPages())

    concoursCat = pywikibot.Category(site, "Article VikiConcours")

    workCat = pywikibot.Category(site, "Article en travaux")

    deadendPagesList = list(site.deadendpages())
    lonelyPagesList = list(site.lonelypages())


    if allPages:
        pagesList = pagegenerators.AllpagesPageGenerator(namespace=0, includeredirects=False, site=site)
    else:
        pagesList = pagegenerators.NewpagesPageGenerator(total=50, site=site)

    for page in pagesList:

        try:
            pageTemp = page.get()

        except pywikibot.exceptions.NoPageError:
            pywikibot.output("Page %s does not exist; skipping."
                             % page.title(as_link=True))
        except pywikibot.exceptions.IsRedirectPageError:
            pywikibot.output("Page %s is a redirect; skipping."
                             % page.title(as_link=True))
        except pywikibot.exceptions.LockedPageError:
            pywikibot.output("Page %s is locked; skipping."
                             % page.title(as_link=True))
        else:


            # On ne s'occupe de la page que si elle n'est ni une homonymie ni une page du VikiConcours ni une page en travaux
            pageCat = list(page.categories())
            if (not homonCat in pageCat) and (not concoursCat in pageCat) and (not workCat in pageCat):

                #On ne traite l'ajout de bandeau que si la page n'est pas ignorée
                jobList = []
                if (not ignoreCat in pageCat) and (not page in ignoreList):

                    # s'il existe des références, on retire le job 'orphelin'
                    if page in lonelyPagesList:
                    #if len(set(page.backlinks(namespaces=0))) < 1:
                        jobList.append('orphelin')

                    # s'il n'existe aucune catégorie (directe), on ajoute le job 'catégoriser'
                    realCat = list(set(pageCat) - set(hiddenCat) - set(ebaucheCat))

                    nbCat = len(list(realCat))
                    if nbCat == 0:
                        jobList.append('catégoriser')

                    # si la page n'appartient à aucun portail, on ajoute le job 'portail'
                    nbPort = len(set(pageCat) & set(portalCat))
                    if nbPort == 0:
                        jobList.append('portail')


                    # si la page ne pointe vers aucune autre, on ajoute le job 'impasse'
                    if page in deadendPagesList:
                    #if len(set(page.linkedPages(namespaces=0))) < 1: ##DESACTIVE TANT QUE LA PAGE SPECIALE NE SE MET PLUS A JOUR##
                        jobList.append('impasse')

                else:
                    pywikibot.output("Page %s in ignore list; skipping."
                                 % page.title(as_link=True))

                pageTemp, oldJobList = removeBanner(pageTemp)
                pywikibot.output("Page [[%s]]" %(page.title()))
                pywikibot.output("oldList: %s\njobList: %s" %(oldJobList, jobList))
                jobList = updateJobList(oldJobList, jobList)
                job = ''

                # Différence symétrique entre les deux listes, on regarde si des éléments ne sont pas contenus dans les deux listes : (A-B)+(B-A)
                diff = list(set(oldJobList).symmetric_difference(set(jobList)))

                if diff != []:
                    nbrTotal += 1
                    if len(jobList) > 0:
                        job = ', '.join(jobList)
                        banner = '{{Maintenance|job=' + job + '|date=~~~~~}}\n\n'
                        pageTemp = banner + pageTemp
                        summary = '[[VD:Robot|Robot]] : Mise à jour du bandeau de maintenance.'
                    else:
                        summary = '[[VD:Robot|Robot]] : Retrait du bandeau de maintenance.'

                    c = callback.Callback()
                    page.text = pageTemp
                    try:
                        page.save(summary, callback=c)
                    except pywikibot.exceptions.PageSaveRelatedError as myexception:
                        pywikibot.output('%s %s'% (type(myexception), myexception.args))

                    if c.error == None:
                        nbrModif += 1

                    log +='*' + '{{Utilisateur:LinedBot/ExtLinker|' + page.title() + '}} : Mise à jour du bandeau {{m|maintenance}} avec les paramètres suivants : ' + job + '\n'

    return log



# Retrait du bandeau si besoin
def removeBanner(pageTemp: str) -> Tuple[str, List[str]]:
    parser = re.compile(r'{{Maintenance\|job=(?P<jb>.*?)(?:\|.*?)?}}(?P<fin>\r\n|\n|\ )', re.I | re.U | re.DOTALL)
    searchResult = parser.search(pageTemp)
    oldJobList = []
    if searchResult:
        jobT = searchResult.group('jb')
        jobT = ''.join(jobT.split()) # on retire tous les espaces de la chaine
        oldJobList = jobT.split(',') # on convertit la chaine en list
        pageTemp = parser.sub('', pageTemp, 1)
    return pageTemp, oldJobList

# Retourne une jobList mise à jour (catégories appliquées par le bot + utilisateurs)
def updateJobList(oldJobList: List[str], newJobList: List[str]) -> List[str]:
    botJobList = ['catégoriser', 'impasse', 'orphelin', 'portail']

    tempJobList = list(oldJobList)
    for j in botJobList:
        if j in oldJobList:
            tempJobList.remove(j)
    newJobList = list(set(newJobList+tempJobList))
    return newJobList


#############
# Execution #
#############
def main():

    allPages = False

    try:
        opts, _ = getopt.getopt(sys.argv[1:], 'a', ['all'])
    except getopt.GetoptError:
        sys.exit(2)

    for opt, _ in opts:
        if opt in ('-a', '--all'):
            allPages = True

    log = ''
    timeStart = time.time()
    log += newPages(allPages)
    timeEnd = time.time()
    logger.setValues(nbrTotal, nbrModif)
    logger.editLog(site, log)
    pywikibot.output("%s (of %s) pages were modified in %s s." %(nbrModif, nbrTotal, round(timeEnd-timeStart, 2)))


if __name__ == "__main__":
    try:
        main()
    finally:
        pywikibot.stopme()
