#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Ce script parcourt la [[Catégorie:Événement récent]] et ses sous-catégories pour retirer le bandeau au bout de 15j
#
# (C) Linedwell, 20211-2025
#
# Distribué sous licence GNU GPLv3
# Distributed under the terms of the GNU GPLv3 license
# http://www.gnu.org/licenses/gpl.html

import sys
sys.path.insert(1, '..') #ajoute au PYTHONPATH le répertoire parent

import mylogging # pylint: disable=unused-import
import utils

import re, time
from datetime import timedelta
import datetime

import sqlite3, hashlib

import pywikibot
from pywikibot import pagegenerators, textlib

import callback # pylint: disable=unused-import
import logger # pylint: disable=unused-import
from typing import Iterator, Tuple, List, Dict # pylint: disable=unused-import

################
# Déclarations #
################
site = pywikibot.Site('fr', 'vikidia')
nbrModif = 0
nbrTotal = 0
#ignoreList = ignoreList.ignoreList
ignorePage = pywikibot.Page(site, "Utilisateur:LinedBot/Ignore")
ignoreList = list(ignorePage.linkedPages())
concoursCat = pywikibot.Category(site, "Article VikiConcours")

#############
# Functions #
#############
# Modification du wiki
def removeTemplate(pagesList: List['pywikibot.page.Page'], catname: str, delay: int, sinceAdd=False, checkTalk=False) -> str:
    global nbrModif, nbrTotal # pylint: disable=global-statement

    log = ''
    summary = ''
    templateFound = False
    limit = utils.calcLimit(delay)
    motif = motifFinder(catname)

    for page in pagesList:
        pageCat = list(page.categories())
        if (not page in ignoreList) and (not concoursCat in pageCat):
            try:
                pageTemp = page.get()

            except pywikibot.exceptions.NoPageError:
                pywikibot.error("Page %s does not exist; skipping."
                             % page.title(as_link=True))
            except pywikibot.exceptions.IsRedirectPageError:
                pywikibot.error("Page %s is a redirect; skipping."
                             % page.title(as_link=True))
            except pywikibot.exceptions.LockedPageError:
                pywikibot.error("Page %s is locked; skipping."
                             % page.title(as_link=True))
            else:

                lastEdit = page.latest_revision.timestamp.replace(tzinfo=datetime.timezone.utc)
                if sinceAdd:
                    added_timestamp = db_check_add(page, motif)
                    if added_timestamp:
                        lastEdit = added_timestamp.replace(tzinfo=datetime.timezone.utc)

                if lastEdit < limit:
                    if checkTalk:
                        talk = page.toggleTalkPage()
                    if not checkTalk or not talk.exists() or talk.latest_revision.timestamp.replace(tzinfo=datetime.timezone.utc) < limit:
                        nbrTotal += 1
                        duration = utils.calcDuration(lastEdit)
                        c = callback.Callback() #(re)init de c
                        for m in motif:
                            parser = re.compile(r'{{\s*?' + m + r'({{.*?}}|.)*?}}(\s*?|(?={{))', re.I | re.U | re.DOTALL)
                            searchResult = parser.search(pageTemp) #On cherche si le motif {{m}} existe dans la page
                            if searchResult:
                                templateFound = True
                                templateResult = searchResult.group()
                                pageTemp = parser.sub('', pageTemp, 1) #Retire la 1re occurrence du motif dans la page

                                templateResult = templateResult.replace('\r\n', '') #Retire les sauts de ligne contenus dans le modèle avant de l'ajouter au résumé
                                templateResult = templateResult.replace('\n', '') #Correspond au second type de retour à la ligne

                                if sinceAdd and added_timestamp:
                                    summary = "Retrait du bandeau %s (ajouté il y a %s jours)." %(templateResult, duration.days)
                                else:
                                    summary = "Retrait du bandeau %s (non modifié depuis %s jours)." %(templateResult, duration.days)

                                c = callback.Callback()
                                page.text = pageTemp
                                try:
                                    page.save("[[VD:Robot|Robot]] : " + summary, callback=c, force=True)
                                    break
                                except pywikibot.exceptions.PageSaveRelatedError as myexception:
                                    pywikibot.error('%s : %s'% (page.title(), myexception.args))
                                    log += " KO: [[%s]] : %s\n" %(page.title(), myexception.args.replace('{{', '{{m|'))
                                    break
                            else:
                                templateFound = False
                                summary = "Aucun modèle trouvé correspondant au motif: " + str(motif)

                        if c.error == None and templateFound:
                            nbrModif += 1
                            status = "OK:"
                        else:
                            status = "KO:"
                        log += "*%s [[%s]] : %s\n" %(status, page.title(), summary.replace('{{', '{{m|'))
                        db_del_entry(page.title(), motif)
        else:
            pywikibot.output("Page %s in ignore list; skipping."
                                 % page.title(as_link=True))

    return log

#Retourne le motif correspondant au(x) modèle(s) catégorisant(s) dans la catégorie donnée
def motifFinder(catname: str) -> List[str]:
    motif = []

    if catname == "Article en travaux":
        motif = ['((Multi|En)[-_ ]?)?travaux', 'En[_ ]cours', 'Trav(ail|vaux(1|4))', 'T']

    return motif

#Retourne la date à laquelle le motif donné est apparu dans la page
#Fonction adaptée de https://github.com/Toto-Azero/Wikipedia/blob/master/pywikibot/mort_recente.py
#(C) Toto Azéro

def find_add(page: 'pywikibot.page.Page', motif: str) -> Tuple['pywikibot.page.User', int, int]:
    regex = [re.compile(m) for m in motif]
    death_found = True
    maxrevision = 100
    history = list(page.revisions(total=maxrevision))
    print(history)

    if len(history) == 1:
        for h in history:
            ident = h['revid']
            timestamp = h['timestamp']
            user = h['user']

        return (pywikibot.User(site, user), ident, timestamp)

    pywikibot.output("=====================================")
    pywikibot.output("Page : %s" % page.title())

    oldid = None
    requester = None
    timestamp = None
    previous_timestamp = None

    for h in history:

        ident = h['revid']
        timestamp = h['timestamp']
        user = h['user']

        pywikibot.output("Analyzing id %i: timestamp is %s and user is %s" % (ident, timestamp, user))
        text = page.getOldVersion(ident)
        try:
            templates_params_list = textlib.extract_templates_and_params(text)
        except Exception:
            pywikibot.error("Skipping id %i; page content is hidden" % (ident))
            continue

        death_found = False
        for (template_name, _) in templates_params_list:
            try:
                template_page = pywikibot.Page(pywikibot.Link(template_name, site, default_namespace=10), site)
                # TODO : auto-finding redirections
                if any(r.match(template_page.title(with_ns=False)) for r in regex):
                    death_found = True
                    break
            except Exception as myexception:
                pywikibot.error('An error occurred while analyzing template %s' % template_name)
                pywikibot.error('%s %s'% (type(myexception), myexception.args))

        if oldid:
            pywikibot.output("id is %i ; oldid is %i" % (ident, oldid)) # pylint: disable=bad-string-format-type
        else:
            pywikibot.output("id is %i ; no oldid" % ident)
        if not death_found:
            if ident == oldid:
                pywikibot.output("Last revision does not contain any %s template!" % motif)
                return None
            else:
                pywikibot.output("-------------------------------------")
                triplet = (requester, oldid, previous_timestamp)
                try:
                    pywikibot.output("Page : %s" % page.title())
                    pywikibot.output("Found it: user is %s; oldid is %i and timestamp is %s" % triplet) # pylint: disable=bad-string-format-type
                except:
                    pass
                return triplet
        else:
            requester = pywikibot.User(site, user)
            oldid = ident
            previous_timestamp = timestamp

    # Si on arrive là, c'est que la version la plus ancienne de la page contenait déjà le modèle
    return (pywikibot.User(site, user), ident, timestamp)

# Récupération des pages de la catégorie
def crawlerCat(catname: str, delay: int, sinceAdd=False, subcat=False, checkTalk=False) -> str:
    log = ''
    cat = pywikibot.Category(site, catname)
    pagesInCat = list(cat.articles(recurse=False))
    pagesList = pagegenerators.PreloadingGenerator(pagesInCat) # On génère la liste des pages incluses dans la catégorie
    pagesList = pagegenerators.NamespaceFilterPageGenerator(pagesList, [0]) #On ne garde que les articles (Namespace 0)
    log += removeTemplate(pagesList, cat.title(with_ns=False), delay, sinceAdd, checkTalk)

    if subcat:
        subcat -= 1
        subcategories = list(cat.subcategories())
        for subc in subcategories:
            log += crawlerCat(subc.title(with_ns=False), delay, sinceAdd, subcat, checkTalk)

    return log

################
# DB Functions #
################
# Vérifie si la date d'ajout du motif est déjà présente en base. Si oui, la retourne, sinon parcourt l'historique de la page
# et ajoute les informations à la base
def db_check_add(page: 'pywikibot.page.Page', motif: str):
    hash_motif = hashlib.md5(str(motif).encode('utf-8')).hexdigest()

    conn = sqlite3.connect('db/recents.db')
    cursor = conn.cursor()
    cursor.execute("""
    SELECT * FROM viki_recents
    WHERE page = ? AND
    pattern = ?
    """, [page.title(), hash_motif])
    result = cursor.fetchone()

    if not result:
        pywikibot.output("No timestamp for %s add to %s in DB; checking page revisions"%(motif, page.title()))
        _, _, added = find_add(page, motif)
        if not added:
            return None
        timestamp = added.strftime("%Y-%m-%d %H:%M:%S")
        cursor.execute("""
        INSERT INTO viki_recents(page, added, pattern) VALUES(?, ?, ?)""", (page.title(), timestamp, hash_motif))
        conn.commit()
        return added
    else:
        pywikibot.output("Timestamp for %s add to %s found in DB"%(motif, page.title()))

    conn.close()

# Supprime de la base les informations sur l'ajout du motif sur la page
def db_del_entry(page: 'pywikibot.page.Page', motif: str):
    hash_motif = hashlib.md5(str(motif).encode('utf-8')).hexdigest()
    conn = sqlite3.connect('db/recents.db')
    cursor = conn.cursor()
    cursor.execute("""
    DELETE FROM viki_recents
    WHERE page = ? AND
    pattern = ?
    """, [page.title(), hash_motif])
    conn.commit()
    conn.close()

# Supprime de la base les entrées plus anciennes que X jours
def db_clean_old(jours: int):
    today = datetime.datetime.now(datetime.UTC)
    older = today - timedelta(seconds=jours * 86400)
    oldfrmt = older.strftime("%Y-%m-%d")
    conn = sqlite3.connect('db/recents.db')
    cursor = conn.cursor()
    cursor.execute("""
    DELETE FROM viki_recents
    WHERE added < ?
    """, (oldfrmt, ))
    conn.commit()
    conn.close()

#############
# Execution #
#############
def main():
    log = ''
    timeStart = time.time()
    log += crawlerCat(catname='Article en travaux', delay=30, checkTalk=True) #30 jours, inclusion des PdD associées aux articles

    timeEnd = time.time()
    logger.setValues(nbrTotal, nbrModif)
    logger.editLog(site, log)
    db_clean_old(90)

    pywikibot.output("%s (of %s) pages were modified in %s s."
                        %(nbrModif, nbrTotal, round(timeEnd-timeStart, 2)))


if __name__ == "__main__":
    try:
        main()
    finally:
        pywikibot.stopme()

